import matplotlib
import logging
from droplet_growth import multiwell

matplotlib.logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def crop_center(array):
    shape = array.shape
    crop =  array[
        shape[0] // 4 : shape[0] * 3 // 4, shape[1] // 4 : shape[1] * 3 // 4,
    ]
    return crop


def segment_bf(well, thr=0.2, smooth=10, erode=10, fill=True, plot=False):
    '''
    Serments input 2d array using thresholded gradient with filling
    Returns SegmentedImage object
    '''
    grad = multiwell.get_2d_gradient(well)
    sm = multiwell.gaussian_filter(grad, smooth)
#     sm = multiwell.gaussian_filter(well, smooth)
    
    regions = sm > thr * sm.max()
    
    if fill:
        regions = multiwell.binary_fill_holes(regions)
    
    if erode:
        regions = multiwell.binary_erosion(regions, iterations=erode)
    
    labels, n_labels = multiwell.label(regions)
    print(f'{n_labels} region{" s"[n_labels>1]}')
    if plot:
        fig, ax = multiwell.plt.subplots(1,3)
        ax[0].imshow(well, cmap='gray')
        ax[0].set_title('input')
        ax[1].imshow(sm, cmap='gray')
        ax[1].set_title('grad')
        ax[2].imshow(labels)
        ax[2].set_title(f'{n_labels} labels')
        multiwell.plt.show()
        
    return labels