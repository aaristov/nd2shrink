FROM jupyter/datascience-notebook:latest
WORKDIR /home/jovyan
USER root
COPY . .
RUN python -V &&\
    # python -m pip install -r requirements.txt &&\
    pip install -e .
USER jovyan
CMD python -V && jupyter notebook