Installation
============

Clone the repository from git 

.. code-block:: console 

    git clone https://gitlab.pasteur.fr/aaristov/nd2shrink.git
    git checkout Develop-multiwell-analysis

and run 

.. code-block:: console 

    cd nd2shrink
    pip install .


Usage
=====

.. code-block:: python

    import tifffile as tf
    from droplet-growth import register
    
    template = tf.imread('path_to_template')
    mask = tf.imread('path_to_mask')

    aligned_stack = register.aligned_stack(path_to_bf_fluo_stack, template, mask)