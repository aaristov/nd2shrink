droplet\_growth.multiwell
=========================

.. automodule:: droplet_growth.multiwell

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      analyse_curves
      estimate_bg
      filter_table_by_max_intensity
      filter_table_by_min_intensity
      get_2d_gradient
      get_background_table
      get_centers
      get_intensity_table
      get_mask
      get_outlines
      plot_intensity_line
      plot_intensity_raw_line
      plot_intensity_vs_time
      read_stitched_nd2
      show
   
   

   
   
   

   
   
   



