nd2tif.read
===========

.. automodule:: nd2tif.read

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      auto_order
      nd2
      tiff
   
   

   
   
   

   
   
   



