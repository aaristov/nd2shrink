nd2tif.transform
================

.. automodule:: nd2tif.transform

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      recursive_downscale
      reshape_like_IJ
      scale_down
      shape
      to_8bits
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ImageJStack
      Well
   
   

   
   
   



