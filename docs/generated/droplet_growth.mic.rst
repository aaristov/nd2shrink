droplet\_growth.mic
===================

.. automodule:: droplet_growth.mic

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      align_and_count
      concatenate_horizontally_results_for_one_chip
      concatenate_vertically_all_chips_csv
      get_cell_numbers
      get_file_pair
      get_intensities
      get_overnight_intensities
      get_stats
      read_align_count
      show_mean_instensities
      show_prob_survive
   
   

   
   
   

   
   
   



