droplet\_growth.fit
===================

.. automodule:: droplet_growth.fit

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      add_doubling_time
      erf_fun
      exp_exp_fit
      exp_exp_fun
      exp_on_baseline
      exponent
      fit_exp_on_baseline
      fit_exponent
      fit_sigmoid
      gompertz
      hill
      lag_exponent
      lin_exp_fit
      lin_exp_fun
      plot_fit
      prob_survive
      show_probs
      sigmoid
      single_prob
      single_prob_to_final_states
   
   

   
   
   

   
   
   



