.. Multidroplet-chip documentation master file, created by
   sphinx-quickstart on Tue Feb  1 19:35:37 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Multidroplet-chip's documentation!
=============================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   usage
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
