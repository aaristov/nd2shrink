API
===

.. autosummary::
   :toctree: generated
   :recursive:
   
   droplet_growth

.. autosummary::
   :toctree: generated
   :recursive:

   nd2tif
   
.. autosummary::
   :toctree: generated
   :recursive:

   segment
   
   