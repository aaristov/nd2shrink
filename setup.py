#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="nd2tools",
    version="0.3.0.dev0",
    description="Compress nd2 into multi-dimensional tiff",
    author="Andrey Aristov",
    author_email="aaristov@pasteur.fr",
    url="https://gitlab.pasteur.fr/aaristov/nd2shrink",
    install_requires=[
        "numpy",
        "scipy",
        "scikit-image",
        "pytest",
        "tifffile",
        "tqdm",
        "pandas",
        "seaborn",
        "statannot",
        "click",
        "imreg-dft",
        "zarr",
        "nd2",
        "dask",
        "fire",
        "aicsimageio"
    ],
    python_requires=">3.7",
    packages=find_packages(),
)
