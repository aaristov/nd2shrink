import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage as ndi
from skimage.filters import threshold_otsu, rank
from skimage.measure import label, regionprops
from skimage.morphology import disk, remove_small_objects
from skimage.util import img_as_ubyte
from skimage.color import label2rgb


def get_local_otsu(image:np.ndarray, radius : int = 300) -> np.ndarray:
    img = img_as_ubyte(image)
    selem = disk(radius)
    local_otsu = rank.otsu(img, selem)
    return local_otsu


def get_mask(image:np.ndarray, threshold:np.ndarray, 
    erosion=3, min_size=5000, figsize=(20,10), plot=False):

    img = img_as_ubyte(image)

    mask = image <= threshold
    mask = ndi.binary_fill_holes(mask)
    mask = ndi.binary_erosion(mask, iterations=erosion)

    mask = remove_small_objects(mask, min_size=min_size)

    if plot:

        plt.subplots(figsize=figsize)
        plt.imshow(label2rgb(mask, image, bg_label=0),)
        plt.colorbar()
    return mask


def filter_mask(mask, size_lim=(80,180), ecc_min=.7, plot=False):
    labels = label(mask)

    regions = regionprops(labels, intensity_image=None)

    bad_regions = list(filter(
        lambda r: r.major_axis_length < size_lim[0] \
            or r.major_axis_length > size_lim[1] \
            or r.eccentricity > ecc_min, regions))

    good_mask = mask.copy()

    if len(bad_regions) > 0:
        for br in bad_regions:
            good_mask[labels == br.label] = 0
    print(f'selecting {label(good_mask).max()}/{labels.max()} regions')
    if plot:
        plt.imshow(label(good_mask))
        plt.show()
    return good_mask