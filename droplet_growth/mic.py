from tifffile import imwrite, imread
from glob import glob
import numpy as np
import droplet_growth.register as reg
from droplet_growth import segment, count, poisson, fit
from nd2tif.transform import Well, to_8bits
import matplotlib.pyplot as plt
import pandas as pd
from skimage.measure import regionprops, regionprops_table
import re
import seaborn as sns



def get_stats(dff_with_final_state):
    '''Collects prob_survive and sample size for all concentrations and n_cells
    Return:
    DataFrame with four columns
    '''
    assert all([c in dff_with_final_state.columns for c in ['n_cells', 'ng', 'final_state']])
    stats = []
    for n in range(1, 6):
        for c in dff_with_final_state['ng'].unique():
            df = dff_with_final_state.query(f'`ng` == {c}').query(f'n_cells == {n}')
            N = len(df)
            p = df.final_state.mean()
            stats.append({'n_cells': n, 'sample_size': N, 'ng': c, 'prob_survive': p})
    
    df = pd.DataFrame(stats)
    
    fig, ax= plt.subplots(dpi=150)
    sns.scatterplot(data=df, x='n_cells', y='prob_survive', hue='ng', size='sample_size', ax=ax,)
    sns.lineplot(data=df, x='n_cells', y='prob_survive', hue='ng', ax=ax, legend=None)
    ax.legend(loc=(1.01,0))

    return df


def show_prob_survive(dff:pd.DataFrame, threshold_mean_intensity='auto', max_cells:int=5, dpi:int=150, title:str=''):

    '''
    Thresholds mean_intensity to get final state and 
    plots average for all concentrations and cell numbers 0..max_cells
    Return:
    DataFrame with new column: final_state and threshold
    '''
    assert all([c in dff.columns for c in ['n_cells', 'mean_intensity', 'ng']])

    if threshold_mean_intensity == 'auto':
        max_c = dff["ng"].max()
        thr = (ddd:= dff.query(f'`ng` == {max_c}').mean_intensity).mean() +  ddd.std()
        print(f'computed threshold = {thr} from max concentration {max_c}')
    else:
        thr = threshold_mean_intensity

    df = dff.copy()
    df.loc[:, 'final_state'] = df.mean_intensity > thr
    df.loc[:, 'threshold_mean_intensity'] = thr

    fig, ax = plt.subplots(dpi=dpi)
    sns.lineplot(
        ax=ax, 
        data=df.query(f'n_cells <= {max_cells}'), 
        x='ng', 
        y='final_state', 
        hue='n_cells', 
        ci=95)
    plt.xlabel('[AB] ng')
    plt.title(title + " prob_survive")
    plt.show()
    return df


def get_intensities(path:str, regex:str=r'(\d+)ng', plot:bool=False, save_suffix='intensities',
    calibration_um=.7):
    '''
    Reads aligned stack from tif (bf, fluo, mask)
    Gets intensities, gets concetration from the filename.
    Returns pandas Dataframe with corresponding columns
    Saves tif stack with [bf, fluo, labels, mean_intensity]
    '''
    stack = imread(path)
    print(path)
    print(stack.shape)
    try:
        bf, fluo, mask = stack
    except ValueError:
        bf, fluo, mask = [stack[:,:,i] for i in range(3)]
    print(fluo.shape)
    fluo = fluo.astype('f')
    # fluo_wo_bg = fluo - segment.ndi.gaussian_filter(fluo, len(fluo)//20)
    # labels = segment.label(segment.ndi.binary_dilation(mask, np.ones((dilate, dilate))))
    labels = segment.label(mask)

    df = pd.DataFrame(
        regionprops_table(
            labels, 
            # fluo_wo_bg, 
            fluo,
            properties=['label', 'mean_intensity', 'max_intensity']
        )
    )
    try:
        ng = re.compile(regex).findall(path)[0]
        print (ng)
        df.loc[:,'ng'] = int(ng)
    except IndexError:
        print('concentration not found')
        
    if plot:
        truncated_fluo = fluo_wo_bg.copy()
        truncated_fluo[truncated_fluo > 100] = 100
        fig, ax = plt.subplots(nrows=3,figsize=(10,10), dpi=150)
        ax[0].imshow(fluo)
        ax[1].imshow(fluo_wo_bg)
        ax[2].imshow(segment.label2rgb(labels>0, to_8bits(truncated_fluo), bg_label=0,))

    if save_suffix is not None:
        intensity_mask = mask
        for i in range(1, labels.max() + 1):
            intensity = df.query(f'label == {i}').mean_intensity
            intensity_mask[labels == i] = int(intensity)
        stack = np.array([bf, fluo, labels, intensity_mask], dtype='uint16')
        _ = Well(stack, order='cyx', calibration_um=calibration_um, ).save_tif(p := path.replace('.tif', f'.{save_suffix}.tif'))
        print(f'Saved {p}')

        df.to_csv(c := path.replace('.tif', f'.{save_suffix}.csv'), index=None)
        print(f'Saved table to {c}')
    return df

def show_mean_instensities(results:pd.DataFrame, max_cells=5, mean_intensity_lim=600, dpi=150, title=''):
    '''swarm plot of mean_intensity vs concenctration colored as n_cells'''
    assert all([c in results.columns for c in ['n_cells', 'mean_intensity', 'concentration, ng']])
    fig, ax = plt.subplots(dpi=dpi)
    sns.swarmplot(
        data=results.query(f'0<n_cells<={max_cells}').query(f'mean_intensity < {mean_intensity_lim}'), 
        x='concentration, ng', 
        y='mean_intensity', 
        hue='n_cells', 
        size=1, 
        dodge=True,
        ax=ax
    )

    ax.set_xlabel('[AB] ng')
    ax.set_title(title + "mean_intensity")
    return ax



def read_align_count(composite_tif_path, template_bin8, mask):
    print(composite_tif_path)
    sample_name = composite_tif_path.split('/')[-2]
    composite = imread(composite_tif_path)
    values = np.mean(composite, axis=(1,2))
    bf = composite[np.argmax(values)]
    fluo = composite[np.argmin(values)]
    
    l, df = align_and_count(bf, fluo, title=sample_name, path=composite_tif_path, 
                            template_bin8=template_bin8,
                            mask=mask)
    del (composite, bf, fluo)
    return {'path': composite_tif_path, 'lambda': l, 'counts': df}


def get_overnight_intensities(overnight_tif_path, aligned_composite_tif_path):
    print(overnight_tif_path, aligned_composite_tif_path)
    try:
        bf_aligned, fluo_aligned, mask = imread(aligned_composite_tif_path)
        bfo, fluoo = imread(overnight_tif_path)
    except ValueError:
        print('No pair')
        return overnight_tif_path, None
    
    plt.figure(dpi=150, figsize=(10,5))
    plt.imshow(bf_aligned, cmap='gray')
    plt.title('Initial bf')
    plt.show()
    
    print('data ready')
    bfo_bin8_filtered = reg.filter_vertical_stripes(bfo[::8,::8], )
    bf_bin8_aligned = bf_aligned[::8,::8]
    print('filtered bfo')
    tvec_bin8 = reg.get_transform(bfo_bin8_filtered, bf_bin8_aligned, dpi=150,   )
    plt.show()
    tvec_bin1 = reg.scale_tvec(tvec_bin8, 8)
    print('tvec ready')
    fluoo_aligned = reg.unpad(reg.transform(fluoo, tvec_bin1), mask.shape)
    
    plt.imshow(fluoo_aligned, vmin=(fm := fluoo_aligned.mean()), vmax = fm + 200 )
    plt.title('aligned fluo')
    plt.show()
    print('fluo aligned')
    df = pd.DataFrame(regionprops_table(segment.label(mask), fluoo_aligned, properties=('mean_intensity', 'max_intensity')))
    df.loc[:, 'bg'] = np.median(fluoo_aligned)
    df.to_csv(overnight_tif_path.replace('Overnight.tif', 'intensities.csv'))
    print('df saved')
    df.mean_intensity.hist(bins=50)
    plt.show()
    return overnight_tif_path, df



def align_and_count(
    bf_bin1:np.ndarray, 
    fluo_bin1:np.ndarray, 
    template_bin8:np.ndarray, 
    mask:np.ndarray, 
    title:str='', 
    rename:tuple=('Composite', 'Aligned_composite'), 
    path:str=None,
    px_size:float=.65,
):
    bf_bin8 = bf_bin1[::8, ::8]
    bf_bin8_filtered = reg.filter_vertical_stripes(bf_bin8)
    print(f'data ready, mask shape {mask.shape}')
    plt.imshow(bf_bin8_filtered, cmap='gray')
    plt.show()
    tr = reg.get_transform(bf_bin8_filtered, template_bin8, plot=True)
    plt.show()
    print('registration ready')
    aligned_fluo = reg.unpad(reg.transform(fluo_bin1, reg.scale_tvec(tr, 8)), mask.shape)
    print(f'fluo aligned {aligned_fluo.shape}')
    aligned_bf = reg.unpad(reg.transform(bf_bin1, reg.scale_tvec(tr, 8)), mask.shape)  
    print(f'bf aligned {aligned_bf.shape}')
    stack = np.stack((aligned_bf, aligned_fluo, mask, ))
    well = Well(stack.astype('uint16'), 'cyx', px_size)
#     plt.imshow(aligned_fluo[:2000,:2000], vmin=440, vmax=600)
#     plt.show()
#     plt.imshow(mask[:2000,:2000])
#     plt.show()
    
    n_cells = get_cell_numbers(aligned_fluo, mask)    
    print('cells counted')
    df = pd.DataFrame(n_cells)
    l  = poisson.fit(df.n_cells, title=title)
    df.loc[:, 'poisson fit'] = l
    if path:
        well.save_tif(path.replace(*rename))
#         imwrite(path.replace(*rename), stack)
        df.to_csv(path.replace('Composite.tif', 'counts.csv'))
    return l, df



def get_cell_numbers(
    multiwell_image:np.ndarray, 
    labels:np.ndarray, 
    plot=False, 
    threshold_abs:float=2, 
    min_distance:float=5,
    meta:dict={},
    bf:np.ndarray=None
    ) -> pd.DataFrame:
    
    props = segment.regionprops(labels)

    def get_n_peaks(i):
        if bf is None:
            return count.get_peak_number(
                multiwell_image[props[i].slice], 
                plot=plot, 
                dif_gauss_sigma=(3, 5), 
                threshold_abs=threshold_abs, 
                min_distance=min_distance,
                title=props[i].label
                )
        else:
            return count.get_peak_number(
                multiwell_image[props[i].slice], 
                plot=plot, 
                dif_gauss_sigma=(3, 5), 
                threshold_abs=threshold_abs, 
                min_distance=min_distance,
                title=props[i].label,
                bf_crop=bf[props[i].slice],
                return_std=True
                )

    n_cells = list(map(get_n_peaks, range(labels.max())))
    return pd.DataFrame([{
        'label': prop.label, 
        'x': prop.centroid[0], 
        'y': prop.centroid[1], 
        'n_cells': n_cell[0],
        # 'std': n_cell[1],
        **meta
        } for prop, n_cell in zip(props, n_cells)])



def get_file_pair(overnight_path):
    c_path = glob(overnight_path.replace('Overnight', 'Aligned_composite'))
    try:
        c_path = c_path[0]
    except IndexError:
        c_path = None
    return overnight_path, c_path



def concatenate_horizontally_results_for_one_chip(
    prefix:str, 
    filenames=('counts.csv', 'intensities.csv'), 
    meta_regexp=r'(\d+e-\d+)_(\d+)ng/',
    meta_columns=('inoculum','concentration, ng'),
    meta_types=(float, int)
    ):
    '''
    Prefix is a folder path in which `counts.csv` and `intensitites.csv` can be found
    This finction reads and concatenated horisontally two files.
    Return:
    pandas.DataFrame with both csv combined
    '''
    try:
        meta_info = re.compile(meta_regexp).findall(prefix)[0]
        print (meta_info)
    
        df = pd.concat((pd.read_csv(prefix + f, index_col=0) for f in filenames), axis=1)
        for column, to_type, meta in zip(meta_columns, meta_types, meta_info):
            df.loc[:, column] = to_type(meta)
        df.loc[:, 'folder'] = prefix
        return df
    except FileNotFoundError as e:
        print(e.args)
        print(f'something is missing in {prefix}')
        return None
    except IndexError as i:
        print(i.args)
        print(f'unable to parce metadata in {prefix}')
        return None
    

def concatenate_vertically_all_chips_csv(
        list_of_folders_with_counts_and_intensities:list, 
        fun=concatenate_horizontally_results_for_one_chip
):
    '''Concatenates vertically results for different concentrations'''
    df = pd.concat(list(filter(lambda x: x is not None, map(fun, list_of_folders_with_counts_and_intensities))))
    df.index  = list(range(len(df)))
    return df