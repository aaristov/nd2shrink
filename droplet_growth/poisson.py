from scipy.optimize import curve_fit
from scipy.stats import poisson
import matplotlib.pyplot as plt
import numpy as np


def fit(numbers:np.ndarray, max_value=None, xlabel='Initial number of cells', 
        title='', plot=True, save_fig_path=None):
    if max_value is None:
        max_value = numbers.max()
    bins = np.arange(max_value + 1) - .5
    vector = bins[:-1] +.5
    hist, bins= np.histogram(numbers, bins=bins, density=True)
    popt, pcov = curve_fit(poisson.pmf, vector, hist, p0=(1.,))
    l = popt[0]
    if plot:
        plt.hist(numbers, bins=bins, fill=None)
        plt.plot(vector, len(numbers) * poisson.pmf(vector, l), '.-', label=f'Poisson fit λ={l:.1f}', color='tab:red')
        plt.xlabel(xlabel)
        plt.title(title)
        plt.legend()
        if save_fig_path is not None:
            try:
                plt.savefig(save_fig_path)
                print(f'Save histogram {save_fig_path}')
            except Exception as e:
                print('saving histogram failed', e.args)
        plt.show()
    return l
