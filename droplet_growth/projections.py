from api import read, Well
import numpy as np
import os
from tifffile import imwrite


def get_max_mean_projections(folder, output, channels:list, bin=1, regex='([ctz])(\\d{1,2})', 
                            prefix:tuple=('max_', 'mean_'), suffix='projection_cX.tif'):
    '''
    Reads single files exported by NIS to the `folder` using regex.
    Extracts max, mean projections for all timepoints and selectes channels.
    Saves tif stacks, returns two stacks as np.arrays
    '''
    reader = read.VirtualStack(folder, regex=regex)
    print(reader)
    t_range = tuple(reader.ranges['t'].values())
    ppp = []
    for c in channels:
        print(f'channel {c}')
        proj = get_t_series(t_range, reader, c, bin)
        try:
            print('Saving data')
            save_projections(proj, output, prefix, suffix=suffix.replace('X', str(c)), )
        except Exception as e:
            print(f'saving failed: {e.args}')
        ppp.append(proj)
    return proj


def _get_z_projections(reader:read.VirtualStack, t:int, c:int=3, bin:int=1):
    ''' Makes max, mean z projection for a single timepoint 
    Return:
    -------
    (numpy.array, numpy.array) - max, mean 2D arrays
    '''
    print('.', end='')
    images = list(reader.read(c=c, t=t, bin=bin))
    stack = Well.stack(images)
    max_proj = stack.array.max(0)
    avg_proj = stack.array.mean(0)
    return max_proj, avg_proj


def get_t_series(t_range:tuple, reader:read.VirtualStack, c:int=3, bin:int=1):
    '''
    Makes max, mean z projection for a range of timepoints,
    including the last index

    Return:
    -------
    (numpy.array, numpy.array) - max, mean 3D arrays
    '''
    print(f'assembling z projections for t-series {t_range}: ')
    fun = lambda t: _get_z_projections(reader, t, c, bin)
    z_projections = list(map(fun, range(t_range[0],t_range[1]+1)))

    max_stack = np.stack([p[0].astype('int16') for p in z_projections])
    avg_stack = np.stack([p[1].astype('float32') for p in z_projections])

    return max_stack, avg_stack


def save_projections(data:tuple, folder:str, prefix:tuple=('max_', 'mean_'), suffix='projection_c3.tif'):
    '''
    Saves 3D time-stacks in ImageJ format
    '''
    for d, p in zip(data, prefix):
        path = os.path.join(folder, f'{p}{suffix}')
        imwrite(path, d.reshape((len(d),1,1,*d.shape[1:],1)), imagej=True)
        print(f'\nsaved data to `{path}`')
    return True


def crop(array:np.ndarray, center:tuple, size:int):

    ''' 2d square crop of last 2 dimensions of 2d/3d array '''

    limits = (c := np.array(center).astype(int)) - size, c + size  
    limits = np.max((np.zeros((2,)), limits[0]), axis=0), \
            np.min((array.shape[-2:], limits[1]), axis=0)

    if array.ndim == 2:
        return array[limits[0][0]:limits[0][1], limits[1][0]:limits[1][1]]
    elif array.ndim == 3:
        return array[:limits[0][0]:limits[0][1], :limits[1][0]:limits[1][1]]

    else:
        raise ValueError(f'Expected 2d or 3d array, got {array.shape}')