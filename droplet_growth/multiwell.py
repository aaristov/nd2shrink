from droplet_growth import fit
import numpy as np
import pims_nd2 as nd
import pandas as pd
import seaborn as sns
import json
import logging
import matplotlib.pyplot as plt
from scipy.ndimage import (gaussian_filter, 
                            binary_erosion, 
                            binary_dilation, 
                            binary_fill_holes, 
                            label, 
                            measurements,
                            laplace
)
from skimage.measure import regionprops, regionprops_table


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def analyse_curves(ID=None, intensity_tables:list=[], 
    dyes:list=[],
    meta_table:pd.DataFrame=None, 
    plot=True, 
    skip=2, 
    fit_fun=fit.fit_exp_on_baseline, 
    gen_fun=fit.exp_on_baseline, 
    start=None, 
    stop:str=None,
    min_len=10):

    '''
    stop:str/int
        'grad': use gradient
        int: use hard limit
        None: Use all data
    '''
    
    print(f'ID: {ID}')
    try:
        if meta_table is not None:
            meta = meta_table.query(f'ID == {ID}').to_dict()
            print(meta)
        else:
            meta = {}

        wells = [table[table.label == ID] for table in intensity_tables]
        # bgs = [bg[bg.label == ID] for bg in bg_tables]
        bg_free_values = [well.I.values for well in wells]

        # if bg_free_values[0].max() < 5 * bg_free_values[0].std():
        #     raise ValueError(f'No signal: bg_free_values.max() < 5 * std()')
        
    except KeyError:
        print(f'Wrong ID {ID}')
        return [{'id': ID, **meta, 'dye': ddd, 'comment': 'Wrong ID'} for ddd in dyes]
    except ValueError as e:
        print(*e.args)
    
        if plot:    
            fig, ax = plt.subplots(1,1, figsize=(5,2), sharex='all')
            
            ax.set_title(f'{dyes[0]} raw')
            ax.plot(wells[0].mean_intensity.values, label='signal')
            ax.plot(wells[0].bg_mean.values, label='bg')
            ax.legend()
            plt.show()
        
        return [{'id': ID, **meta, 'dye': dye, 'curve': well.I, 'comment': e.args} for dye, well in zip(dyes, wells)]

    grad = np.gradient(wells[0].mean_intensity.values)
    peak_grad = None
    
    if stop == 'grad':
        try:    
            peak_grad = np.argmax(grad[:np.argmin(grad)])
            stop = peak_grad
            print(f'Using gradient peak {stop} as stop')
            if peak_grad < min_len:
                raise ValueError('dataset too short')
        except ValueError:
            print('dataset too short')
            return [{'id': ID, **meta, 'dye': ddd, 'comment': 'dataset too short'} for ddd in dyes]
   
    elif isinstance(stop, int) or stop is None:
        pass
    else:
        raise ValueError('stop parameter {stop} not understood: use integer, `grad`, or None')


    if plot:
        try:
            wells[0].intensity_image.shape
            for dye, well in zip(dyes, wells):
                print(dye)
                fig, ax = plt.subplots(1, len(well) // skip, sharex='all', sharey='all', figsize=(15,1))
                for _a, (i, img) in zip(ax, list(enumerate(well.intensity_image))[::skip]):
                    img[img==0] = img.mean()
                    _a.imshow(img)
                    _a.set_title(i)
                #     a.colorbar()
            plt.show()
        except AttributeError:
            print('No images, ignoring plot')

        
        fig, ax = plt.subplots(3,1, figsize=(5,5), sharex='all')
        for dye, well in zip(dyes[:1], wells[:1]):
            ax[0].set_title(f'{dye} raw')
            ax[0].plot(well.mean_intensity.values, label='signal')
            ax[0].plot(well.bg_mean.values, label='bg')
            ax1 = ax[0].twinx()
            if peak_grad:
                ax1.plot(grad, color='tab:green', label='gradient')
                ax1.plot(peak_grad, grad[peak_grad], 'go', label='peak of gradient')
            ax[0].legend()
            ax1.legend(loc=(1,.5))
    
    

    try:
        popts = []
        datas = []
        for data in bg_free_values:
            popt = fit_fun(data[start:stop])
            datas.append(data[start:stop] - popt[0])
            print(popt)
            popts.append(popt)
        
        fit_results = [gen_fun(np.arange(len(datas[0])), 0, *popt[1:]) for popt in popts]
        
        chi2s = [np.sum((fit_result - data) ** 2 ) / (len(fit_result) - 2) \
            for fit_result, data in zip (fit_results, datas)]
        [print(f'chi2: {chi2}') for chi2 in chi2s]
        if plot:
            ax[1].set_title('intensities wo bg')
            ax[1].plot(np.arange(start, stop), datas[0], label=dyes[0], color='tab:orange')
            ax[1].set_ylabel(dyes[0], color='tab:orange')
            ax2 = ax[1].twinx()
            if len(dyes) > 1:
                ax2.plot(np.arange(start, stop), datas[1], label=dyes[1], color='tab:blue')
                ax2.set_ylabel(dyes[1], color='tab:blue')

            for aa, fit_result, dye, color in zip((ax[1], ax2), fit_results, dyes, ('tab:green', 'tab:red')):
                aa.plot(np.arange(start, stop), fit_result, '.', label=f'{dye}_fit', color=color)

            ax[2].set_title('intensities wo bg log scale')
            ax[2].semilogy(datas[0], label=dyes[0])
            ax[2].set_ylabel(dyes[0], color='tab:orange')
            ax3 = ax[2].twinx()
            if len(dyes) > 1:
                ax3.semilogy(datas[1], label=dyes[1], color='tab:orange')
                ax3.set_ylabel(dyes[1], color='tab:blue')

            for aa, fit_result, dye, color in zip((ax[2], ax3), fit_results, dyes, ('tab:green', 'tab:red')):
                aa.semilogy(fit_result, '.', label=f'{dye}_fit', color=color)
            plt.show()

        return [
            {'id': ID, 
            **meta, 
            'dye': dye, 
            **{key: val for key, val in zip('abcdef', popt)}, 
            'chi2': chi2, 
            'curve': bg_free_value,
            'stop': stop,
            'max_grad': max(grad) 
            } 
            for dye, popt, chi2, bg_free_value in zip(dyes, popts, chi2s, bg_free_values)
            ]

    except (RuntimeError, TypeError) as e:
        print('No fit', *e.args)
        print(bg_free_values)
        if plot:
            ax[1].set_title('intensities wo bg')
            ax[1].plot(np.arange(start, stop), bg_free_values[0], label=dyes[0])
            ax[1].set_ylabel(dyes[0], color='tab:blue')
            ax2 = ax[1].twinx()
            if len(dyes) > 1:
                ax2.plot(np.arange(start, stop), bg_free_values[1], label=dyes[1], color='tab:orange')
                ax2.set_ylabel(dyes[1], color='tab:orange')

    if plot:
            plt.show()
    return [{'id': ID, **meta, 'dye': dye} \
            for dye in dyes]
            


def get_intensity_table(
    labelled_mask: np.ndarray,
    intensity_image_sequence: np.ndarray,
    values = ['mean_intensity', ],
    estimator = np.mean,
    plot: bool = True
):
    assert (iis := intensity_image_sequence).ndim == 3, (
        f'expected 3D stack for intensity, got shape {iis.shape}'
    )
    df = pd.DataFrame(columns=['time', 'label', *values])

    for t, img in enumerate(iis):
        dict_li = regionprops_table(
            labelled_mask,
            intensity_image=img,
            properties=['label', *values]
        )
        dict_bg = regionprops_table(
            get_outlines(labelled_mask),
            intensity_image=img,
            properties=['mean_intensity']
        )
        dict_litb = {'time': [t]*len(dict_li['label']), **dict_li, 'bg_mean': dict_bg['mean_intensity']}
        df1 = pd.DataFrame.from_dict(dict_litb)
        df = pd.concat([df, df1])

    if plot:
        plot_intensity_vs_time(df)
    df.loc[:, "I"] = df.mean_intensity - df.bg_mean
    return df


def estimate_bg(values, eps=.1):
    '''iteratively shaves off the values higher than mean + 3 * std to get a good mean value''' 
    mean, std = values.mean(), values.std()
    good_values = values[values < mean + 3 * std]
    new_mean = good_values.mean()
    if np.abs(new_mean - mean) > eps:
        return estimate_bg(good_values)
    else:
        return new_mean     


def get_outlines(labels):
    '''creates 1 px outline around labels'''
    return labels * (np.abs(laplace(labels)) > 0)


def get_background_table(
    labelled_mask: np.ndarray,
    intensity_image_sequence: np.ndarray,
    estimator = estimate_bg,
    plot: bool = True
):
    """
    Makes outlines out of well mask and estimates signals
    """
    assert (iis := intensity_image_sequence).ndim == 3, (
        f'expected 3D stack for intensity, got shape {iis.shape}'
    )
    df = pd.DataFrame(columns=['time', 'label', 'mean_intensity'])
    
    outlines = get_outlines(labelled_mask)

    for t, img in enumerate(iis[:]):
        print('.', end='')
        def get_vals(label):
            mask = outlines == label
            intensities = img[mask]
            return {'time': t, 'label': label, 'mean_intensity': estimator(intensities)}
        labels = np.unique(labelled_mask)
        labels = labels[labels > 0]
        vals = list(map(get_vals, labels))
        df1 = pd.DataFrame(data=vals)
        df = pd.concat([df, df1])

    if plot:
        plot_intensity_vs_time(df)
    return df

def get_centers(labelled_mask:np.ndarray, plot=True, shift_labels=(0,30), color='w', size=6, figsize=(10,6), dpi=150):
    '''
    Find the centroid coordinates of the labelled mask

    Return:
    -------
    centers: list of dict {'center': (y,x), 'label': int}
    '''

    assert labelled_mask.ndim == 2, 'Please provide 2D array with labels'

    mask = labelled_mask
    centers = [{'center': r.centroid, 'label': r.label} for r in regionprops(mask)]

    if plot:
        fig, ax = plt.subplots(figsize=(10,6), dpi=150)
        plt.imshow(mask > 0)
        for c in centers:
            (cy, cx), label = c.values()
            plt.text(cx + shift_labels[0], cy + shift_labels[1], label, fontdict={'color': color, 'size': size,})
        plt.axis('off')

    return centers


def filter_table_by_min_intensity(
    table: pd.DataFrame, column: str, time: int,
    min_intensity: float
):
    '''
    Returns rows which at time `time` have `mean_intensity` > `min_intensity`
    '''
    return table[table[table.time == time].loc[:, column] > min_intensity]


def filter_table_by_max_intensity(
    table: pd.DataFrame, column: str, time: int,
    max_intensity: float
):
    '''
    Returns rows which at time `time` have `column` < `max_intensity`
    '''
    return table[table[table.time == time].loc[:, column] < max_intensity]


def read_stitched_nd2(path: str, bundle="zyx", channel=0, time_limit=None):
    """
    Reads nd2 with bundle, channel
    Yields one timepoint at a time.
    """

    with nd.ND2_Reader(path,) as frames:
        logger.info(frames.sizes)
        # logger.info(frames.calibration)
        # logger.info(frames.calibrationZ)

        # json.dump(frames.metadata, open(path.replace('.nd2','_meta.json'), 'w'), default=repr)
        frames.iter_axes = "t"
        frames.default_coords["c"] = channel
        frames.bundle_axes = bundle
        for zyx in frames[:time_limit]:
            yield zyx


def get_mask(
    bf_image: np.ndarray,
    thr:float=0.1,
    sigma:float=2,
    erode: int = 10,
    post_dilate: int = 10,
    size_lim : tuple = (None, None),
    eccentricity_lim: float = 0.7,
    plot=True,
    figsize=(5, 5),
    structure=None
) -> np.ndarray:
    '''
    Creates a labelled mask using first image from bf stack
    '''
    assert bf_image.ndim == 2, f'expected 2D image, got {bf_image.shape}'
    bf = bf_image

    mask = _detect_wells(bf, erode=erode, thr=thr, sigma=sigma)
    labels, n_labels = label(mask)
    regions = regionprops(labels, intensity_image=None)
    print(len(regions), ' regions')

    if not any(size_lim):
        sizes = np.array([r.major_axis_length for r in regions])
        mean_sizes = sizes.mean()
        std_sizes = sizes.std()
        size_lim = (mean_sizes - std_sizes, mean_sizes + std_sizes)
    else:
        mean_sizes = np.mean(size_lim)

    bad_regions = list(filter(lambda r: r.major_axis_length < size_lim[0] or r.major_axis_length > size_lim[1] or r.eccentricity > eccentricity_lim, regions))
    print((lb := len(bad_regions)), ' bad regions')
    
    good_mask = mask.copy()

    if lb:
        bad_mask = np.max([labels == br.label for br in bad_regions], axis=0)
        good_mask[bad_mask] = 0

    if post_dilate:
        good_mask = binary_dilation(good_mask, structure=structure, iterations=post_dilate)

    good_labels, n  = label(good_mask)

    print(f'{n} good regions')

    if plot:

        show(bf, vmax=bf.max(), figsize=figsize)
        plt.title('input')
        plt.show()

        h, bins, _ = plt.hist([r.major_axis_length for r in regions], bins=30)
        plt.vlines(mean_sizes, ymin=0, ymax=h.max())
        plt.vlines(size_lim[0], ymin=0, ymax=h.max() / 3)
        plt.vlines(size_lim[1], ymin=0, ymax=h.max() / 3)
        plt.title('size')
        plt.show()

        plt.hist([r.eccentricity for r in regions], bins=30)
        plt.title('eccentricit  y')
        plt.show()
        
        if lb:
            show(bad_mask, figsize=figsize)
            plt.title('bad regions')
            plt.show()
        

        show(good_mask, figsize=figsize)
        plt.title('good mask')
        plt.show()

        show(good_labels, figsize=figsize)
        plt.title('good labels')
        plt.show()

    return good_labels


def _detect_wells(bf: np.ndarray, thr=0.1, sigma=2, erode=5, plot=False):
    '''
    Return mask of wells
    '''
    grad = get_2d_gradient(bf)
    sm_grad = gaussian_filter(grad, sigma)
    mask = sm_grad > sm_grad.max() * thr
    filled_mask = binary_erosion(
        binary_fill_holes(mask),
        structure=np.ones((erode, erode))
    )

    if plot:
        [show(b) for b in [grad, sm_grad, mask, filled_mask]]

    return filled_mask


def get_2d_gradient(xy):
    gx, gy = np.gradient(xy)
    return np.sqrt(gx ** 2 + gy ** 2)


def show(grad, figsize=(5, 5), **kwargs):
    plt.figure(figsize=figsize)
    plt.imshow(grad, cmap='gray', **kwargs)


def plot_intensity_vs_time(
    table: pd.DataFrame, x='time', y='mean_intensity', size=1, **kwargs
):
    ''' Swarm-plot '''
    plot = sns.swarmplot(data=table, x=x, y=y, size=size, **kwargs)
    return plot


def plot_intensity_line(
    table: pd.DataFrame, x='time', y='mean_intensity', **kwargs
):
    ''' Averaged line plot with confidence interval '''
    plot = sns.lineplot(data=table, x=x, y=y, ci='sd', **kwargs)
    return plot


def plot_intensity_raw_line(
    table: pd.DataFrame, x='time', y='mean_intensity', units='label', **kwargs
):
    ''' Plot every dataset in the series as a separate line '''
    plot = sns.lineplot(data=table, x=x, y=y, units=units, estimator=None, **kwargs)
    return plot
