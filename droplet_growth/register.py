from tifffile import imread, imwrite
import matplotlib.pyplot as plt
import imreg_dft as reg
import numpy as np
import scipy.ndimage as ndi
from droplet_growth import mic
from nd2tif.transform import to_8bits
import os

grey = np.array([np.arange(256)] * 3, dtype='uint8')
red = np.array([np.arange(256), np.zeros((256,)), np.zeros((256,))], dtype='uint8')
green = np.array([np.zeros((256,)), np.arange(256), np.zeros((256,))], dtype='uint8')
blue = np.array([np.zeros((256,)), np.zeros((256,)), np.arange(256)], dtype='uint8')

META_ALIGNED = {'ImageJ': '1.53c',
 'images': 3,
 'channels': 3,
 'hyperstack': True,
 'mode': 'composite',
 'unit': '',
 'loop': False,
 'min': 1878.0,
 'max': 30728.0,
 'Ranges': (1878.0, 30728.0, 430.0, 600.0, 0.0, 501.0),
 'LUTs': [grey, green, blue]
 }

def align_stack(data_or_path, template16, mask2, plot=False, path_to_save=None, binnings=(1,16,2), suffix='.aligned.tif'):
    '''
    stack should contain two channels: bright field and fluorescence.
    BF will be binned 8 times and registered with template8 (aligned BF).
    When the transformation verctor will be applied to the original data and stacked with the mask.
    The output stack is of the same size as mask.
    The resulting 3-layer stack will be returned and also saved with suffix ".aligned.tif"
    

    :return: aligned (bf, fluo, mask)
    :rtype: np.ndarray
    
    '''
    if isinstance(data_or_path, str):
        path = data_or_path
        stack = imread(path)
        print(path, stack.shape)
    else:
        assert data_or_path.ndim == 3 and data_or_path.shape[0] == 2
        assert path_to_save is not None, "Please provide the path to save the data"
        stack = data_or_path

    print(f'Aligned stack will be saved to {path_to_save}')

    bf, tritc = stack[:2]
    stack_temp_scale = binnings[1] // binnings[0]
    mask_temp_scale = binnings[1] // binnings[2]
    stack_mask_scale = binnings[2] // binnings[0]
    
    f_bf = bf[::stack_temp_scale, ::stack_temp_scale]

    # f_bf = filter_by_fft(
    #     bf[::stack_temp_scale, ::stack_temp_scale], 
    #     sigma=40,
    #     fix_horizontal_stripes=True, 
    #     fix_vertical_stripes=True,
    #     highpass=True
    # )
    tvec8 = get_transform(f_bf, template16, plot=plot)
    plt.show()
    tvec = scale_tvec(tvec8, mask_temp_scale)
    print(tvec)
    try:
        aligned_tritc = unpad(transform(tritc[::stack_mask_scale, ::stack_mask_scale], tvec), mask2.shape)
        aligned_bf = unpad(transform(bf[::stack_mask_scale, ::stack_mask_scale], tvec), mask2.shape)
    except ValueError as e:
        print("stack_mask_scale: ", stack_mask_scale)
        print(e.args)
        raise e
    
    if plot:
        plt.figure(dpi=300)
        plt.imshow(aligned_tritc, cmap='gray',)# vmax=aligned_tritc.max()/5)
        plt.colorbar()
        plt.show()

        saturated_tritc = aligned_tritc.copy()
        saturated_tritc[saturated_tritc > 500] = 500
        plt.figure(dpi=300)
        plt.imshow(mic.segment.label2rgb(mic.segment.label(mask2), to_8bits(saturated_tritc), bg_label=0))
        plt.show()

    aligned_stack = np.stack((aligned_bf, aligned_tritc, mask2)).astype('uint16')

    imwrite(path_to_save, aligned_stack, imagej=True, metadata=META_ALIGNED)
    print(f'Saved aligned stack {path_to_save}')
    return aligned_stack, tvec


def align_stack_nd(stack, template16, mask2, path=None, plot=False, binnings=(1,16,2), suffix='.aligned.tif'):
    '''
    stack should contain two channels: bright field and fluorescence.
    BF will be binned 8 times and registered with template8 (aligned BF).
    When the transformation verctor will be applied to the original data and stacked with the mask.
    The output stack is of the same size as mask.
    The resulting 3-layer stack will be returned and also saved with suffix ".aligned.tif"
    '''

    # print(stack.shape)

    bf, tritc = stack[:2]

    print(f'Aligning {path}: \n', *[f'{name}: {im.shape},  {bin}\n' for name, im, bin in zip(['bf', 'tmp', 'mask'], [bf, template16, mask2], binnings )])

    stack_temp_scale = binnings[1] // binnings[0]
    mask_temp_scale = binnings[1] // binnings[2]
    stack_mask_scale = binnings[2] // binnings[0]
    
    f_bf = bf[::stack_temp_scale, ::stack_temp_scale]

    #filter_by_fft(
    #     bf[::stack_temp_scale, ::stack_temp_scale], 
    #     sigma=40,
    #     fix_horizontal_stripes=True, 
    #     fix_vertical_stripes=True,
    #     highpass=True
    # )
    tvec8 = get_transform(f_bf, template16, plot=plot)
    plt.show()
    tvec = scale_tvec(tvec8, mask_temp_scale)
    print(tvec)
    try:
        aligned_tritc = unpad(transform(tritc[::stack_mask_scale, ::stack_mask_scale], tvec), mask2.shape)
        aligned_bf = unpad(transform(bf[::stack_mask_scale, ::stack_mask_scale], tvec), mask2.shape)
    except ValueError as e:
        print("stack_mask_scale: ", stack_mask_scale)
        print(e.args)
        raise e
    
    if plot:
        plt.figure(dpi=300)
        plt.imshow(aligned_tritc, cmap='gray',)# vmax=aligned_tritc.max()/5)
        plt.colorbar()
        plt.show()

        saturated_tritc = aligned_tritc.copy()
        saturated_tritc[saturated_tritc > 500] = 500
        plt.figure(dpi=300)
        plt.imshow(mic.segment.label2rgb(mic.segment.label(mask2), to_8bits(saturated_tritc), bg_label=0))
        plt.show()

    aligned_stack = np.stack((aligned_bf, aligned_tritc, mask2)).astype('uint16')

    if path is not None:
        imwrite((p:="".join((*path.split('.')[:-1] , suffix))), aligned_stack, imagej=True, metadata=META_ALIGNED)
        print(f'Saved aligned stack {p}')
    return aligned_stack, tvec


def align_timelapse(bf, fluo_stack, template16, mask2, plot=False, binnings=(4,16,2)):
    '''
    stack should contain two channels: bright field and fluorescence.
    BF will be binned 8 times and registered with template8 (aligned BF).
    When the transformation verctor will be applied to the original data and stacked with the mask.
    The output stack is of the same size as mask.
    Return:
     (aligned_bf, aligned_fluo, mask)
    '''

    stack_temp_scale = binnings[1] // binnings[0]
    mask_temp_scale = binnings[1] // binnings[2]
    mask_bf_scale = binnings[0] // binnings[2]
    
    f_bf = bf[::stack_temp_scale, ::stack_temp_scale]
    # f_bf = filter_by_fft(
    #     bf[::stack_temp_scale, ::stack_temp_scale], 
    #     sigma=40,
    #     fix_horizontal_stripes=True, 
    #     fix_vertical_stripes=True,
    #     highpass=True
    # )
    tvec8 = get_transform(f_bf, template16, plot=plot)
    plt.show()
    tvec = scale_tvec(tvec8, stack_temp_scale)
    print(tvec)

    mask = mask2[::mask_bf_scale, ::mask_bf_scale]
    aligned_bf = unpad(transform(bf, tvec), mask.shape)
    
    if plot:
        plt.figure(dpi=300)
        plt.imshow(bf, cmap='gray',)# vmax=aligned_tritc.max()/5)
        plt.colorbar()
        plt.title('Original image')
        plt.show()

        plt.figure(dpi=300)
        plt.imshow(mic.segment.label2rgb(mask, to_8bits(aligned_bf), bg_label=0))
        plt.title('Aligned mask over aligned image')
        plt.show()

    aligned_fluo = list(map(lambda x: unpad(transform(x, tvec), mask.shape), fluo_stack))

    return (aligned_bf, aligned_fluo, mask)


def align_mask_to_bf(bf_path, template16, mask2, plot=False, binnings=(4,16,2)):
    '''
    bf_path sould contain tif with bright field.
    BF will be binned and used as a template to aligh template8.
    When the transformation verctor will be applied to the mask which will be returned 
    resized to the size of the BF input.
    The aligned mask will be saved as "mask.aligned.tif"
    '''

    bf = imread(bf_path)
    print(bf_path, bf.shape)

    bf_temp_scale = binnings[1] // binnings[0]
    mask_temp_scale = binnings[1] // binnings[2]
    mask_bf_scale = binnings[0] // binnings[2]
    
    f_bf = filter_by_fft(
        bf[::bf_temp_scale, ::bf_temp_scale], 
        sigma=40,
        fix_horizontal_stripes=True, 
        fix_vertical_stripes=True,
        highpass=True
    )
    tvec8 = get_transform(pad(template16, f_bf.shape), f_bf, plot=plot)
    plt.show()
    tvec = scale_tvec(tvec8, mask_temp_scale)
    print(tvec)

    padded_mask = pad(mask2[::mask_bf_scale, ::mask_bf_scale], bf.shape)
    aligned_mask = unpad(transform(padded_mask, tvec), bf.shape)
    
    if plot:
        plt.figure(dpi=300)
        plt.imshow(bf, cmap='gray',)# vmax=aligned_tritc.max()/5)
        plt.colorbar()
        plt.title('Original image')
        plt.show()

        plt.figure(dpi=300)
        plt.imshow(tvec8["timg"], cmap='gray',)# vmax=aligned_tritc.max()/5)
        plt.colorbar()
        plt.title('Aligned template')
        plt.show()

        plt.figure(dpi=300)
        plt.imshow(mic.segment.label2rgb(aligned_mask, to_8bits(bf), bg_label=0))
        plt.title('Aligned mask over orginal image')
        plt.show()

    save_path = bf_path.replace('.tif', '.mask.aligned.tif')
    imwrite(save_path, aligned_mask)
    print(f"Saved {save_path}")
    
    return aligned_mask


def get_transform(image, template, plot=True, pad_ratio=1.2, figsize=(10,5), dpi=300):
    '''
    Pads image and template, registers and returns tvec
    '''
    padded_template = pad(template, (s := increase(image.shape, pad_ratio)))
    padded_image = pad(image, s)
    tvec = register(padded_image, padded_template)
    if plot:
        aligned_bf = unpad(tvec['timg'], template.shape)
        plt.figure(figsize=figsize, dpi=dpi)
        plt.imshow(aligned_bf, cmap='gray')
    return tvec


def register(image, template):
    '''
    Register image towards template
    Return:
    tvec:dict
    '''
    assert np.array_equal(image.shape, template.shape), \
        f'unequal shapes {(image.shape, template.shape)}'
    return reg.similarity(template, image, constraints={'scale': [1,0.2], 'tx': [0, 500], 'ty': [0, 500], 'angle': [0, 30]})


def filter_by_fft(
    image, 
    sigma=40, 
    fix_horizontal_stripes=False,
    fix_vertical_stripes=False,
    highpass=True
    ):

    size = get_fft_size(image.shape)
    fft = np.fft.fft2(pad(image, size))
    bg = fft.copy()[0,0]
    if fix_vertical_stripes:
        fft[0, 1:] = 0 #vertical
    if fix_horizontal_stripes:
        fft[1:, 0] = 0 #horizontal
    # fft = fft * np.fft.fftshift(fft_mask(size, sigma = size[0]/sigma, highpass=highpass))
    fft = fft * (fft_mask(size, sigma = size[0]/sigma, highpass=highpass))
    filtered_image = unpad(abs(np.fft.ifft2(fft)), image.shape) 
    return filtered_image

def fft_mask(shape, sigma=10, highpass=False):
    y,x = np.indices(shape)
    
    gaus = np.exp( - (x - shape[1] // 2) ** 2 / sigma ** 2 / 2) * np.exp( - (y - shape[0] // 2) ** 2 / sigma ** 2 / 2) 
    gaus = gaus / gaus.max()
    if highpass:
        return 1 - gaus
    return gaus


def pad(image:np.ndarray, to_shape:tuple=None, padding:tuple=None):
    if padding is None:
        padding = calculate_padding(image.shape, to_shape)
    try:
        padded = np.pad(image, padding, 'edge')
    except TypeError as e:
        print(e.args, padding)
        raise e
    return padded


def unpad(image:np.ndarray, to_shape:tuple=None, padding:tuple=None):
    if any(np.array(image.shape) - np.array(to_shape) < 0):
        print(f'unpad:warning: image.shape {image.shape} is within to_shape {to_shape}')
        image = pad(image, np.array((image.shape, to_shape)).max(axis=0))
        print(f'new image shape after padding {image.shape}')
    if padding is None:
        padding = calculate_padding(to_shape, image.shape)
    
    y = [padding[0][0], -padding[0][1]]
    if y[1] == 0:
        y[1] = None
    x = [padding[1][0], -padding[1][1]]
    if x[1] == 0:
        x[1] = None
    return image[y[0]:y[1], x[0]:x[1]]


def get_fft_size(shape):
    max_size = np.max(shape)
    n = 5
    while (2 ** n) < (max_size * 1.5):
        n += 1
    return (2 ** n, 2 ** n)


def calculate_padding(shape1:tuple, shape2:tuple):
    '''
    Calculates padding to get shape2 from shape1
    Return:
    2D tuple of indices
    '''
    dif = np.array(shape2) - np.array(shape1)
    assert all(dif >= 0), f'Shape2 must be bigger than shape1, got {shape2}, {shape1}'
    mid = dif // 2
    rest = dif - mid
    y = mid[0], rest[0]
    x = mid[1], rest[1]
    return y, x


def scale_tvec(tvec, scale=8):
    tvec_8x = tvec.copy()
    tvec_8x['tvec'] = tvec['tvec'] * scale
    try:
        tvec_8x['timg'] = None
    except KeyError:
        pass
    finally:
        return tvec_8x
    
    
def transform(image, tvec):
    print(f'transform {image.shape}')
    fluo = reg.transform_img_dict(image, tvec)
    return fluo.astype('uint')


def increase(shape, increase_ratio):
    assert increase_ratio > 1
    shape = np.array(shape)
    return tuple((shape * increase_ratio).astype(int))
